import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppComponent } from './app.component';
import { CoreModule } from './core/core.modules';
import { CoverageUploaderModule } from './coverage-uploader/coverage-uploader.module';
import { UploadCoverageService } from './coverage-uploader/services/api/upload-coverage.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    CoverageUploaderModule
  ],
  providers: [UploadCoverageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
