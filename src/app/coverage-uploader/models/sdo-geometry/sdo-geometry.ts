export class SDOGeometry {
    gtype: number;
    srid: number | null;
    point: [number, number, number] | null
    elementInfo: number[] | null;
    ordinates: number[] | null;

    constructor() {
        this.gtype = 0
        this.point = null
        this.elementInfo = null
        this.ordinates = null
        this.srid = 4326
    }
}