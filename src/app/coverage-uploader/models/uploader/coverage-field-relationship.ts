export class CoverageFieldRelationShip {
    country: "56" | "51"
    id: string
    cellname: string
    type: "INDOOR" | "OUTDOOR"

    constructor() {
        this.country = "56"
        this.id = ""
        this.cellname = ""
        this.type = "INDOOR"
    }
}