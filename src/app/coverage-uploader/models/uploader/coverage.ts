import { SDOGeometry } from "../sdo-geometry/sdo-geometry";

export class Coverage {
    country: "56" | "51"
    id: string;
    cellname: string;
    geometry: SDOGeometry | null;

    constructor() {
        this.country = "56"
        this.id = ""
        this.cellname = ""
        this.geometry = null
    }
}