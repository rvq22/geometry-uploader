import { Coverage } from "./coverage";

export class CoverageBatch {
    coverages: Coverage[]
    length: number
    type : "INDOOR" | "OUTDOOR"

    constructor() {
        this.coverages = []
        this.length = 0
        this.type = "INDOOR"
    }
}