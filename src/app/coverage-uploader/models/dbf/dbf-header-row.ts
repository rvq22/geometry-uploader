export interface DbfHeaderRow {
    name: string
    dataType: 'N' | 'F' | 'O' | 'D' | 'L' | string
    len: number
    decimal: number
}