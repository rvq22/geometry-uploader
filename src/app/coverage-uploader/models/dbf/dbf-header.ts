export class DbfHeader {
    lastUpdated: Date | null
    records: number
    headerLength: number
    recLen: number

    constructor() {
        this.lastUpdated = null
        this.records = 0
        this.headerLength = 0
        this.recLen = 0
    }
}