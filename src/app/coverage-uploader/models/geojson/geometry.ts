export class Geometry {
    bbox?: [number, number, number, number] | number[]
    coordinates: number[] | number[][] | number[][][] | number[][][][]
    type: "Point" | "LineString" | "Polygon" | "MultiPoint" | "MultiLineString" | "MultiPolygon" | string

    constructor() {
        this.bbox = []
        this.coordinates = []
        this.type = ""
    }
}