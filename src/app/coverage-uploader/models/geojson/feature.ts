import { Geometry } from "./geometry";

export interface Feature {
    geometry: Geometry
    type: 'Feature'
    properties: any
}
