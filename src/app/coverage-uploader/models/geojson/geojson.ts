import { Feature } from "./feature"

export class GeoJSON {
    features: Feature[]
    fileName?: string | undefined
    type: "FeatureCollection"

    constructor() {
        this.features = []
        this.type = "FeatureCollection"
    }
}
/** Geometry Details:
 * Multipolygon => number[][][][]
 * Polygon => number[][][]
 * Point => number[]
 * LineString => number[][]
 */

