import { ShapeFileHeader } from "./shapefile-header";
import { ShapeFileRecord } from "./shapefile-record";

export class ShapeFileBinaryDescriptor {
    name: string;
    size: number;
    header: ShapeFileHeader | undefined;
    records: ShapeFileRecord[];

    constructor() {
        this.name = ""
        this.size = 0;
        this.header = undefined;
        this.records = []
    }
}