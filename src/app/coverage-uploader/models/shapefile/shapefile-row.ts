export interface ShapeFileRow {
    id: number;
    len: number;
    data: Buffer | null
    type: number
}