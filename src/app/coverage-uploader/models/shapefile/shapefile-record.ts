export interface ShapeFileRecord {
    id: number;
    size: number,
    offset: number;
    type: number;
}