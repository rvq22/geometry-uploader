export interface ShapeFileHeader {
    shpLength: number
    version: number
    type: number
    bbox: [number, number, number, number]
}