import { Feature } from "../geojson/feature"

export class ShapeFile {
    public name: string
    public length: number
    public attributes: string[]
    public features: Feature[]

    constructor() {
        this.name = ""
        this.length = 0
        this.attributes = []
        this.features = []
    }
}