import { Coverage } from "../uploader/coverage";

export class UploadCoverageResponse {
    code: number;
    message: string
    coverages: Coverage[]

    constructor() {
        this.code = 0
        this.message = ""
        this.coverages = []
    }
}