import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UploadCoverageResponse } from '../../models/api/upload-coverage-response';
import { CoverageBatch } from '../../models/uploader/coverage-batch';

@Injectable()
export class UploadCoverageService {

    private endpoint = environment.backend.endpoints.uploadCoverage

    constructor(private http: HttpClient) {

    }

    public uploadCoverageBatch(batch: CoverageBatch) {
        console.log(this.endpoint)
        return this.http.post<UploadCoverageResponse>(this.endpoint, batch);
    }
}