import { ShapeFileHeader } from "../models/shapefile/shapefile-header";
import { ShapeFileRow } from "../models/shapefile/shapefile-row";

declare const Buffer: any
declare const AsyncGenerator: any
declare const AsyncIterableIterator: any;

export class ShapeFileBinaryReader {

    public static async getHeader(file: File | Blob): Promise<ShapeFileHeader> {
        let buffer = Buffer.from(await file.slice(0, 100).arrayBuffer())

        return {
            shpLength: buffer.readInt32BE(6 << 2), // Byte 24
            version: buffer.readInt32LE(7 << 2), // Byte 28
            type: buffer.readInt32LE(8 << 2), // Byte 36
            bbox: [
                buffer.readDoubleLE(9 << 2),
                buffer.readDoubleLE(11 << 2),
                buffer.readDoubleLE(13 << 2),
                buffer.readDoubleLE(15 << 2)
            ]
        }
    }

    public static async getType(file: File | Blob) {
        return (await this.getHeader(file)).type;
    };

    public static async getRow(file: File | Blob, offset: number): Promise<ShapeFileRow> {
        let view = Buffer.from(await file.slice(offset, offset + 12).arrayBuffer())
        let len = view.readInt32BE(4) << 1;
        let data = Buffer.from(await file.slice(offset + 12, offset + len + 8).arrayBuffer())

        return {
            id: view.readInt32BE(0),
            len: len,
            data: data,
            type: view.readInt32LE(8)
        };
    };

    public static async countRecords(shpBlob: Blob) {
        let shpSize = shpBlob.size;
        let currentShpRow: ShapeFileRow;
        let offset = 100;
        let count = 0;

        while (offset < shpSize) {
            currentShpRow = await ShapeFileBinaryReader.getRow(shpBlob, offset);
            offset += 8;
            offset += currentShpRow.len;
            count += 1;
        }
        return count
    }
}