import { Feature } from "../models/geojson/feature";
import { SDOGeometry } from "../models/sdo-geometry/sdo-geometry";

export class GeometryConverter {

    private static createSDOGType(feature: Feature, removeZOrdinate = true) {
        let gtype = 0;
        let coords;

        switch (feature.geometry.type) {
            case "Polygon":
                coords = feature.geometry.coordinates as number[][][]
                gtype = ((1000) * (removeZOrdinate ? 2 : coords[0][0].length)) + 3
                return gtype;
            case "MultiPolygon":
                coords = feature.geometry.coordinates as number[][][][]
                gtype = ((1000) * (removeZOrdinate ? 2 : coords[0][0][0].length)) + 7
                return gtype;
            default:
                console.log("Tipo de Geometría no admitida")
                return gtype
        }
    }

    private static createPolygonElementInfo(coordinates: number[][][]): number[] {
        let length = coordinates.length
        let elemInfo: number[] = []
        let firstCoordinateGroup = false;
        let startingOffset = 0

        for (let i = 0; i < length; i++) {

            if (!firstCoordinateGroup) {
                startingOffset += 1
                elemInfo.push(1)
                firstCoordinateGroup = true
                elemInfo.push(1003)
            } else {
                startingOffset += (coordinates[i - 1].length * 2)
                elemInfo.push(startingOffset)
                elemInfo.push(2003)
            }
            elemInfo.push(1)
        }

        return elemInfo;
    }

    private static createPolygonOrdinates(coordinates: number[][][], removeZOrdinate = true): number[] {
        let ordinates: number[] = []

        coordinates.forEach((polyCoordinates) => {
            polyCoordinates.forEach((coords) => {
                if (removeZOrdinate) {
                    ordinates.push(...coords.splice(0, 2))

                } else {
                    coords.forEach((c) => {
                        ordinates.push(c);
                    });
                }
            });
        });

        return ordinates;
    }

    private static createSDOPolygon(feature: Feature): SDOGeometry | null {

        let geometry: SDOGeometry | null = null

        if (feature.geometry.type === 'Polygon') {
            geometry = new SDOGeometry()
            geometry.ordinates = this.createPolygonOrdinates(feature.geometry.coordinates as number[][][])
            geometry.elementInfo = this.createPolygonElementInfo(feature.geometry.coordinates as number[][][])
            geometry.gtype = this.createSDOGType(feature)
        }

        return geometry
    }

    private static createSDOMultiPolygon(feature: Feature): SDOGeometry | null {

        let geometry: SDOGeometry | null = null

        if (feature.geometry.type === 'MultiPolygon') {
            geometry = new SDOGeometry()
            geometry.ordinates = this.createMultiPolygonOrdinates(feature.geometry.coordinates as number[][][][])
            geometry.elementInfo = this.createMultiPolygonElementInfo(feature.geometry.coordinates as number[][][][])
            geometry.gtype = this.createSDOGType(feature)
        }

        return geometry
    }

    private static createMultiPolygonElementInfo(coordinates: number[][][][]): number[] {
        let elemInfo: number[] = []
        let exteriorPolygon: boolean;
        let startingOffset = 0
        let previousLength = 0
        let firstCoordinateGroup = true

        coordinates.forEach(coordinateGroup => {
            exteriorPolygon = true;

            coordinateGroup.forEach(subCoordinateGroup => {

                if (firstCoordinateGroup) {
                    startingOffset = 1
                    firstCoordinateGroup = false
                } else {
                    startingOffset += (previousLength * 2)
                }
                elemInfo.push(startingOffset)

                if (exteriorPolygon) {
                    elemInfo.push(1003)
                    exteriorPolygon = false
                } else {
                    elemInfo.push(2003)
                }

                elemInfo.push(1)
                previousLength = subCoordinateGroup.length
            })
        })

        return elemInfo;
    }

    private static createMultiPolygonOrdinates(coordinates: number[][][][], removeZOrdinate = true): number[] {
        let ordinates: number[] = []

        coordinates.forEach((multiPolyCoordinates) => {
            ordinates.push(...this.createPolygonOrdinates(multiPolyCoordinates))
        })
        return ordinates;
    }

    public static generateSDOGeometry(feature: Feature, removeZOrdinate = true) {
        let sdoGeometry: SDOGeometry | null = null

        switch (feature.geometry.type) {
            case "Polygon":
                sdoGeometry = this.createSDOPolygon(feature)
                break;
            case "MultiPolygon":
                sdoGeometry = this.createSDOMultiPolygon(feature)
                break
            default:
                console.log("Tipo de Geometría no admitida")
        }

        return sdoGeometry
    }

    public generateSDOMultiPolygon(feature: Feature) {

    }
}