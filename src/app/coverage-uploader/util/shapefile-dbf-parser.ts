import { DbfHeader } from '../models/dbf/dbf-header'
import { DbfHeaderRow } from '../models/dbf/dbf-header-row'
import { BufferDecoder } from './buffer-decoder';

export class ShapeFileBbfParser {

    public static parseHeader(data: Buffer): DbfHeader {
        let header = new DbfHeader();
        header.lastUpdated = new Date(data.readUInt8(1) + 1900, data.readUInt8(2), data.readUInt8(3));
        header.records = data.readUInt32LE(4);
        header.headerLength = data.readUInt16LE(8);
        header.recLen = data.readUInt16LE(10);
        return header;
    }

    public static async getHeader(data: File | Blob): Promise<DbfHeader> {
        let buffer = Buffer.from(await data.arrayBuffer())
        return await ShapeFileBbfParser.parseHeader(buffer)
    }

    public static parseHeaderRows(data: Buffer, headerLength: number, encoding: string | undefined = undefined): DbfHeaderRow[] {
        let out: DbfHeaderRow[] = [];
        let offset = 32;

        while (offset < headerLength) {

            out.push({
                name: BufferDecoder.createDecoder(data.slice(offset, offset + 11), encoding),
                dataType: String.fromCharCode(data.readUInt8(offset + 11)),
                len: data.readUInt8(offset + 16),
                decimal: data.readUInt8(offset + 17),
            });

            if (data.readUInt8(offset + 32) === 13) {
                break;
            } else {
                offset += 32;
            }
        }
        return out;
    }

    public static async getHeaderRows(data: File | Blob, headerLength: number, encoding: string | undefined = undefined): Promise<DbfHeaderRow[]> {
        let buffer = Buffer.from(await data.arrayBuffer())

        return await ShapeFileBbfParser.parseHeaderRows(buffer, headerLength, encoding)
    }

    public static parseDataField(buffer: Buffer, offset: number, len: number, type: 'N' | 'F' | 'O' | 'D' | 'L' | string, encoding: string | undefined = undefined) {
        let data = buffer.slice(offset, offset + len);
        let textData = BufferDecoder.createDecoder(data, encoding)

        switch (type) {
            case 'N':
            case 'F':
            case 'O':
                return parseFloat(textData);
            case 'D':
                return new Date(parseInt(textData.slice(0, 4)), parseInt(textData.slice(4, 6), 10) - 1, parseInt(textData.slice(6, 8)));
            case 'L':
                return textData.toLowerCase() === 'y' || textData.toLowerCase() === 't';
            default:
                return textData;
        }
    }

    public static parseRow(buffer: Buffer, offset: number, headerRows: DbfHeaderRow[]) {
        let out: any = {};
        let i = 0;
        let len = headerRows.length;
        let field;
        let header: DbfHeaderRow;

        while (i < len) {
            header = headerRows[i];
            field = ShapeFileBbfParser.parseDataField(buffer, offset, header.len, header.dataType);
            offset += header.len;
            if (typeof field !== 'undefined') {
                out[header.name] = field;
            }
            i++;
        }
        return out;
    }
}