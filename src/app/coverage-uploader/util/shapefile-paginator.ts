import { GeoJSON } from '../models/geojson/geojson'
import { DbfHeader } from "../models/dbf/dbf-header";
import { DbfHeaderRow } from "../models/dbf/dbf-header-row";
import { Geometry } from "../models/geojson/geometry";
import { ShapeFileHeader } from "../models/shapefile/shapefile-header";
import { ShapeFileRow } from "../models/shapefile/shapefile-row";
import { ShapeFileBinaryReader } from "./shapefile-binary-reader";
import { ShapeFileBbfParser } from "./shapefile-dbf-parser";
import { ShapeFileGeometryParser } from "./shapefile-geometry-parser";

class PaginationIndex {
    pageNumber: number
    shpOffset: number
    dbfOffset: number

    constructor() {
        this.pageNumber = 0
        this.shpOffset = 0
        this.dbfOffset = 0
    }
}

export class ShapeFilePaginator {
    pageInterval: number
    currentPage: number
    countPages: number
    records: any[]
    indexes: PaginationIndex[]
    shpHeader: ShapeFileHeader | undefined
    shpCurrentOffset: number
    hasShp: boolean
    dbfHeader: DbfHeader | undefined
    dbfHeaderRows: DbfHeaderRow[]
    dbfCurrentOffset: number
    hasDbf: boolean

    constructor() {
        this.pageInterval = 0
        this.pageInterval = 50
        this.currentPage = 0
        this.countPages = 0
        this.records = []
        this.indexes = []
        this.shpCurrentOffset = 0
        this.hasShp = false
        this.dbfHeaderRows = []
        this.dbfCurrentOffset = 0
        this.hasDbf = false

        this.indexes.push(new PaginationIndex())
        this.indexes[0].pageNumber = 0
    }

    public static async initPaginator(shpBlob: Blob, dbfBlob: Blob) {
        let paginator = new ShapeFilePaginator()
        await paginator.initShpDefinitions(<Blob>shpBlob)
        await paginator.initDbfDefinitions(<Blob>dbfBlob)
        paginator.countPages = await ShapeFileBinaryReader.countRecords(shpBlob)

        await console.log(paginator.countPages)
        return paginator
    }

    public async initShpDefinitions(shpBlob: Blob) {
        this.shpHeader = await ShapeFileBinaryReader.getHeader(shpBlob)
        this.indexes[0].shpOffset = 100
        this.shpCurrentOffset = this.indexes[0].shpOffset;
        this.hasShp = true;
    }

    public async initDbfDefinitions(dbfBlob: Blob) {
        this.dbfHeader = await ShapeFileBbfParser.getHeader(dbfBlob)
        this.dbfHeaderRows = await ShapeFileBbfParser.getHeaderRows(dbfBlob, this.dbfHeader.headerLength - 1);
        this.indexes[0].dbfOffset = ((this.dbfHeaderRows.length + 1) << 5) + 2;
        this.dbfCurrentOffset = this.indexes[0].dbfOffset
        this.hasDbf = true
    }

    public async getNext(shpBlob: Blob, dbfBlob: Blob) {
        if (this.hasShp) {
            if (this.currentPage < this.countPages) {

                let value = await this.getGeoJSON(shpBlob, dbfBlob)
                this.currentPage += 1;

                await this.createPaginatorIndex(this.currentPage, this.shpCurrentOffset, this.dbfCurrentOffset)

                console.log("Page:" + this.currentPage)
                console.log(value)
            }
        }
    }

    public async getPrevious(shpBlob: Blob, dbfBlob: Blob) {
        if (this.hasShp) {
            if ((this.currentPage - 1) > 0) {
                this.currentPage -= 1;

                this.shpCurrentOffset = this.indexes[this.currentPage - 1].shpOffset
                this.dbfCurrentOffset = this.indexes[this.currentPage - 1].dbfOffset

                let value = await this.getGeoJSON(shpBlob, dbfBlob)

                console.log("Page:" + this.currentPage)
                console.log(value)
            }
        }
    }

    private createPaginatorIndex(page: number, shpOffset: number, dbfOffset: number) {
        if (this.indexes.find(index => index.pageNumber === this.currentPage) === undefined) {

            let index = new PaginationIndex()
            index.pageNumber = page
            index.shpOffset = shpOffset
            index.dbfOffset = dbfOffset
            this.indexes.push(index)
        }
    }

    private async getGeoJSON(shpBlob: Blob, dbfBlob: Blob) {

        let geojson = new GeoJSON()

        let geometries = await this.getGeometries(shpBlob)
        let properties = await this.getProperties(dbfBlob)

        geometries?.forEach((geometry, index) => {
            if (geometry !== null) {
                let prop = properties === null ? null : properties[index]
                geojson.features.push({ type: "Feature", geometry, properties: prop })
            }

        })

        return geojson
    };

    private async getGeometries(shpBlob: Blob) {
        let len = shpBlob.size;
        let rows: Array<Geometry | null> = [];
        let currentShpRow: ShapeFileRow;
        let count = 0

        while (this.shpCurrentOffset < len) {

            currentShpRow = await ShapeFileBinaryReader.getRow(shpBlob, this.shpCurrentOffset);
            this.shpCurrentOffset += 8;
            this.shpCurrentOffset += currentShpRow.len;

            if (currentShpRow.type && currentShpRow.data !== null) {
                rows.push(ShapeFileGeometryParser.parseGeometry(currentShpRow.data, await ShapeFileBinaryReader.getType(shpBlob)));
            } else {
                rows.push(null);
            }

            count += 1;

            if (count === this.pageInterval) {
                break;
            }

        }
        return rows
    }

    public async getProperties(dbfblob: Blob, encoding: string | undefined = undefined) {
        let buffer = Buffer.from(await dbfblob.arrayBuffer())
        let header = await ShapeFileBbfParser.parseHeader(buffer);
        let headerRows = await ShapeFileBbfParser.parseHeaderRows(buffer, header.headerLength - 1, encoding);

        let recLen = header.recLen;
        let records: number = (this.dbfHeader as DbfHeader).records;
        let out: any[] = [];
        let count = 0

        while (records) {
            out.push(await ShapeFileBbfParser.parseRow(buffer, this.dbfCurrentOffset, headerRows));
            this.dbfCurrentOffset += recLen;

            records--;

            count += 1;

            if (count === this.pageInterval) {
                break;
            }
        }
        return out;
    };
}