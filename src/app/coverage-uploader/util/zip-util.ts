
declare const zip: any;

export class ZipUtil {

    public static getZipEntries(file: File, options: any) {
        return (new zip.ZipReader(new zip.BlobReader(file))).getEntries(options);
    }

    public static async getEntryBlob(entry: any, options: any): Promise<Blob> {
        return await entry.getData(new zip.BlobWriter(), options) as Blob
    }

    public static async forEachEntryBlobs(selectedFile: File | Blob, encoding: string, callback: (filename: string, blob: Blob) => void) {
        let entries = await ZipUtil.getZipEntries(selectedFile as File, { encoding: encoding });
        if (entries && entries.length) {
            for (let entry of entries) {
                await callback(entry.filename as string, await ZipUtil.getEntryBlob(entry, encoding))
            }
        }
    }
}