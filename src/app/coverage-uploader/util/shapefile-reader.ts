import { GeoJSON } from "../models/geojson/geojson";
import { ShapeFile } from "../models/read-shapefile/shapefile";

declare const shp: any;

export class ShapeFileReader {

    private static readFile(file: File) {
        let fileReader: FileReader = new FileReader();
        fileReader.readAsArrayBuffer(file);
        return new Promise((resolve, reject) => {

            fileReader.onloadend = () => {
                resolve(fileReader.result)
            }

            fileReader.onerror = () => {
                reject(fileReader.error)
            }
        })
    }

    private static async parseToGeoJSON(shpFile: File): Promise<GeoJSON> {
        return shp(await this.readFile(shpFile));
    }

    private static getAttributes(shpFile: GeoJSON): string[] {

        return Object.keys(shpFile.features[0].properties)
    }

    public static async read(shpFile: File): Promise<ShapeFile | null> {
        let geojson = await this.parseToGeoJSON(shpFile)

        if (geojson.features.length > 0) {
            let shp = new ShapeFile()
            shp.name = geojson.fileName
            shp.length = geojson.features.length
            shp.attributes = this.getAttributes(geojson)
            shp.features = geojson.features
            return shp
        }

        console.log("El ShapeFile no contiene registros")
        return null
    }


}