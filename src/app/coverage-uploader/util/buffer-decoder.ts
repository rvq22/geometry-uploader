import { StringDecoder } from "string_decoder";
import { encodingExists, decode } from 'iconv-lite'

export class BufferDecoder {


    private static getDefaultDecoder(data: Buffer): string {
        let decoder = new StringDecoder();
        let out = decoder.write(data) + decoder.end();
        return out.replace(/\0/g, '').trim();
    }

    public static createDecoder(buffer: Buffer, encoding?: string): string {
        if (!encoding) {
            return BufferDecoder.getDefaultDecoder(buffer);
        }
        if (!encodingExists(encoding)) {
            if (encoding.length > 5 && encodingExists(encoding.slice(5))) {
                encoding = encoding.slice(5);
            } else {
                return BufferDecoder.getDefaultDecoder(buffer);
            }
        }
        return BufferDecoder.getIconVDecoder(buffer, encoding);


    }

    private static getIconVDecoder(buffer: Buffer, encoding: string): string {
        let out = decode(buffer, encoding);
        return out.replace(/\0/g, '').trim();
    }
}