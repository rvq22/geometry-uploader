import { Coverage } from "../models/uploader/coverage";
import { Feature } from "../models/geojson/feature";
import { CoverageFieldRelationShip } from "../models/uploader/coverage-field-relationship";
import { GeometryConverter } from "./geometry-converter";

export class CoverageUtil {

    public static featureToCoverage(feature: Feature, fieldRelationShip: CoverageFieldRelationShip) {

        let coverage = new Coverage()

        coverage.country = fieldRelationShip.country
        coverage.id = feature.properties[fieldRelationShip.id]
        coverage.cellname = feature.properties[fieldRelationShip.cellname]
        coverage.geometry = GeometryConverter.generateSDOGeometry(feature)


        return coverage
    }
}