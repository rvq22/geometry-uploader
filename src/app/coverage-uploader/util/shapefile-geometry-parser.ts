import { Geometry } from "../models/geojson/geometry";

export class ShapeFileGeometryParser {

    public static parseGeometry(data: Buffer, type: number, removeZOrdinate = true): Geometry {

        switch (type) {
            case 1:
                return ShapeFileGeometryParser.parsePoint(data);
            case 3:
                return ShapeFileGeometryParser.parsePolyline(data);
            case 5:
                return ShapeFileGeometryParser.parsePolygon(data);
            case 8:
                return ShapeFileGeometryParser.parseMultiPoint(data);
            case 11:
                return ShapeFileGeometryParser.parseZPoint(data, removeZOrdinate);
            case 13:
                return ShapeFileGeometryParser.parseZPolyline(data, removeZOrdinate);
            case 15:
                return ShapeFileGeometryParser.parseZPolygon(data, removeZOrdinate);
            case 18:
                return ShapeFileGeometryParser.parseZMultiPoint(data, removeZOrdinate);
            default:
                throw new Error('I don\'t know that shp type');
        }
    }

    private static isClockWise(numberArray: number[][]): boolean {
        let sum = 0;
        let i = 1;
        let len = numberArray.length;
        let prev, cur;
        while (i < len) {
            prev = cur || numberArray[0];
            cur = numberArray[i];
            sum += ((cur[0] - prev[0]) * (cur[1] + prev[1]));
            i++;
        }
        return sum > 0;
    }

    private static polyReduce(acumulate: number[][][][], b: number[][]): number[][][][] {
        if (ShapeFileGeometryParser.isClockWise(b) || !acumulate.length) {
            acumulate.push([b]);
        } else {
            acumulate[acumulate.length - 1].push(b);
        }
        return acumulate;
    }

    private static parsePolygon(data: Buffer): Geometry {
        return ShapeFileGeometryParser.handlePolygon(ShapeFileGeometryParser.parsePolyline(data));
    };
    private static parseZPolygon(data: Buffer, removeZOrdinate = false): Geometry {
        return ShapeFileGeometryParser.handlePolygon(ShapeFileGeometryParser.parseZPolyline(data, removeZOrdinate));
    };

    private static handlePolygon(geometry: Geometry): Geometry {
        if (geometry.type === 'LineString') {
            geometry.type = 'Polygon';
            (geometry.coordinates as number[][][]) = [geometry.coordinates as number[][]];
            return geometry;

        } else {
            geometry.coordinates = (geometry.coordinates as number[][][]).reduce(ShapeFileGeometryParser.polyReduce, []);
            if (geometry.coordinates.length === 1) {
                geometry.type = 'Polygon';
                geometry.coordinates = geometry.coordinates[0];
                return geometry;
            } else {
                geometry.type = 'MultiPolygon';
                return geometry;
            }
        }
    };

    private static checkOffset(offset: number, ext: number, length: number): void {
        if ((offset % 1) !== 0 || offset < 0) throw new RangeError('offset is not uint')
        if (offset + ext > length) throw new RangeError('Trying to access beyond buffer length')
    }

    private static parsePoint(data: Buffer): Geometry {
        return {
            'type': 'Point',
            'coordinates': ShapeFileGeometryParser.parseCoordinates(data, 0)
        };
    };
    private static parseZPoint(data: Buffer, removeZOrdinate = false): Geometry {
        let pointXY = ShapeFileGeometryParser.parsePoint(data);

        if (!removeZOrdinate) {
            (pointXY.coordinates as number[]).push(data.readDoubleLE(16));
        }

        return pointXY;
    };


    private static parseCoordinates(data: Buffer, offset: number): number[] {
        return [data.readDoubleLE(offset), data.readDoubleLE(offset + 8)];
    }

    private static parsePointArray(data: Buffer, offset: number, num: number): number[][] {
        let out = [];
        let done = 0;
        while (done < num) {
            out.push(ShapeFileGeometryParser.parseCoordinates(data, offset));
            offset += 16;
            done++;
        }
        return out;
    };
    private static parseZPointArray(data: Buffer, zOffset: number, num: number, coordinates: number[][], removeZOrdinate = false): number[][] {
        let i = 0;
        while (i < num) {
            if (!removeZOrdinate) {
                coordinates[i].push(data.readDoubleLE(zOffset));
            }
            i++;
            zOffset += 8;
        }
        return coordinates;
    };
    private static parseArrayGroup(data: Buffer, offset: number, partOffset: number, num: number, tot: number): number[][][] {
        let out = [];
        let done = 0;
        let curNum, nextNum = 0,
            pointNumber;
        while (done < num) {
            done++;
            partOffset += 4;
            curNum = nextNum;
            if (done === num) {
                nextNum = tot;
            } else {
                nextNum = data.readInt32LE(partOffset);
            }
            pointNumber = nextNum - curNum;
            if (!pointNumber) {
                continue;
            }
            out.push(ShapeFileGeometryParser.parsePointArray(data, offset, pointNumber));
            offset += (pointNumber << 4);
        }
        return out;
    };

    private static parseZArrayGroup(data: Buffer, zOffset: number, num: number, coordinates: number[][][], removeZOrdinate = false): number[][][] {
        let i = 0;
        while (i < num) {
            coordinates[i] = ShapeFileGeometryParser.parseZPointArray(data, zOffset, coordinates[i].length, coordinates[i], removeZOrdinate);
            zOffset += (coordinates[i].length << 3);
            i++;
        }
        return coordinates;
    };

    private static parseMultiPoint(data: Buffer): Geometry {
        let geometry = new Geometry();
        let mins = ShapeFileGeometryParser.parseCoordinates(data, 0);
        let maxs = ShapeFileGeometryParser.parseCoordinates(data, 16);
        geometry.bbox = [
            mins[0],
            mins[1],
            maxs[0],
            maxs[1]
        ];
        ShapeFileGeometryParser.checkOffset(32, 4, data.length)
        let num = data.readInt32LE(32);
        let offset = 36;
        if (num === 1) {
            geometry.type = 'Point';
            geometry.coordinates = ShapeFileGeometryParser.parseCoordinates(data, offset);
        } else {
            geometry.type = 'MultiPoint';
            geometry.coordinates = ShapeFileGeometryParser.parsePointArray(data, offset, num);
        }
        return geometry;
    };

    private static parseZMultiPoint(data: Buffer, removeZOrdinate = false): Geometry {
        let geometry = ShapeFileGeometryParser.parseMultiPoint(data);
        let num;
        if (geometry.type === 'Point') {
            (geometry.coordinates as number[]).push(data.readDoubleLE(72));
            return geometry;
        } else {
            num = (geometry.coordinates as number[][]).length;
        }

        let zOffset = 52 + (num << 4);
        geometry.coordinates = ShapeFileGeometryParser.parseZPointArray(data, zOffset, num, geometry.coordinates as number[][], removeZOrdinate);
        return geometry;
    };

    private static parsePolyline(data: Buffer): Geometry {
        let geometry = new Geometry();
        let mins = ShapeFileGeometryParser.parseCoordinates(data, 0);
        let maxs = ShapeFileGeometryParser.parseCoordinates(data, 16);
        geometry.bbox = [
            mins[0],
            mins[1],
            maxs[0],
            maxs[1]
        ];
        let numParts = data.readInt32LE(32);
        let num = data.readInt32LE(36);
        let offset, partOffset;

        if (numParts === 1) {
            geometry.type = 'LineString';
            offset = 44;
            geometry.coordinates = ShapeFileGeometryParser.parsePointArray(data, offset, num);
        } else {
            geometry.type = 'MultiLineString';
            offset = 40 + (numParts << 2);
            partOffset = 40;
            geometry.coordinates = ShapeFileGeometryParser.parseArrayGroup(data, offset, partOffset, numParts, num);
        }
        return geometry;
    };

    private static parseZPolyline(data: Buffer, removeZOrdinate = false): Geometry {
        let geoJson = ShapeFileGeometryParser.parsePolyline(data);
        let num = geoJson.coordinates.length;
        let zOffset;

        if (geoJson.type === 'LineString') {
            zOffset = 60 + (num << 4);
            geoJson.coordinates = (geoJson.coordinates as number[][])
            geoJson.coordinates = ShapeFileGeometryParser.parseZPointArray(data, zOffset, num, geoJson.coordinates as number[][], removeZOrdinate);
            return geoJson;

        } else {
            geoJson.coordinates = (geoJson.coordinates as number[][][])

            let totalPoints = geoJson.coordinates.reduce((a: number, v: number[][]) => {
                return a + v.length;
            }, 0);

            zOffset = 56 + (totalPoints << 4) + (num << 2);
            geoJson.coordinates = ShapeFileGeometryParser.parseZArrayGroup(data, zOffset, num, geoJson.coordinates, removeZOrdinate);
            return geoJson;
        }
    };
}