import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchUploaderComponent } from './batch-uploader.component';

describe('BatchUploaderComponent', () => {
  let component: BatchUploaderComponent;
  let fixture: ComponentFixture<BatchUploaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BatchUploaderComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchUploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
