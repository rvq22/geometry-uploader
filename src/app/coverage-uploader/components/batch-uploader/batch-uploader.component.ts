import { Component, Input, OnInit } from '@angular/core';
import { Coverage } from '../../models/uploader/coverage';
import { ShapeFile } from '../../models/read-shapefile/shapefile';
import { CoverageBatch } from '../../models/uploader/coverage-batch';
import { CoverageFieldRelationShip } from '../../models/uploader/coverage-field-relationship';
import { CoverageUtil } from '../../util/coverage-util';
import { UploadCoverageService } from '../../services/api/upload-coverage.service';

@Component({
  selector: 'app-batch-uploader',
  templateUrl: './batch-uploader.component.html',
  styleUrls: ['./batch-uploader.component.scss']
})
export class BatchUploaderComponent implements OnInit {

  private batchIntervals = 500;
  private count = 0;
  private interval = 0

  constructor(private uploadCoverageService: UploadCoverageService) { }

  ngOnInit(): void { }

  async upload(shapefile: ShapeFile, fieldRelationShip: CoverageFieldRelationShip) {
    let batch = await this.generateBatch(shapefile, fieldRelationShip).next()
    console.log(batch.value)

    //await this.uploadCoverageService.uploadCoverageBatch(batch.value as CoverageBatch).subscribe(response => {
    //  console.log(response)
    //});
  }

  private * generateBatch(shapefile: ShapeFile, fieldRelationShip: CoverageFieldRelationShip) {

    let batch = new CoverageBatch()

    while (shapefile.length > this.count) {

      if (shapefile.length <= this.batchIntervals) {
        this.interval += shapefile.length
      }
      else {
        if ((this.count + this.batchIntervals) > shapefile.length) {
          this.interval = shapefile.length - this.count
        }
        else {
          this.interval = this.batchIntervals
        }
      }
      batch.length = this.interval
      batch.coverages = this.generateCoverages(this.count, this.interval, shapefile, fieldRelationShip)
      batch.type = fieldRelationShip.type

      this.count += this.interval
      console.log("Count: " + this.count)

      yield batch
    }

    return true
  }

  private generateCoverages(count: number, interval: number, shapeFile: ShapeFile, fieldRelationShip: CoverageFieldRelationShip): Coverage[] {
    return shapeFile.features.slice(count, (count + interval)).map(feature => {
      return CoverageUtil.featureToCoverage(feature, fieldRelationShip)
    })
  }
}
