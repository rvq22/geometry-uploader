import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoverageUploaderComponent } from './coverage-uploader.component';

describe('CoverageUploaderComponent', () => {
  let component: CoverageUploaderComponent;
  let fixture: ComponentFixture<CoverageUploaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CoverageUploaderComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverageUploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
