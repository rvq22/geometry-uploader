import { Component, OnInit, ViewChild } from '@angular/core';
import { ShapeFile } from '../../models/read-shapefile/shapefile';
import { CoverageFieldRelationShip } from '../../models/uploader/coverage-field-relationship';
import { BatchUploaderComponent } from '../batch-uploader/batch-uploader.component';

@Component({
  selector: 'app-coverage-uploader',
  templateUrl: './coverage-uploader.component.html',
  styleUrls: ['./coverage-uploader.component.scss']
})
export class CoverageUploaderComponent implements OnInit {

  public currentShapeFile: ShapeFile | null = null

  @ViewChild(BatchUploaderComponent)
  private batchUploaderComponent: BatchUploaderComponent | undefined

  public readShapeFileFinished = false

  constructor() { }

  ngOnInit(): void {

  }

  public canLoadShapefiles() {
    return (typeof Worker !== 'undefined')
  }

  public initWebWorker() {
    let worker = new Worker('../../workers/read-shapefile.worker', { type: 'module' })
    worker.onmessage = ({ data }) => {
      console.log(`page got message: ${data}`);
    };

    return worker;
  }

  public onReadShapeFileFinished(shp: ShapeFile | null) {
    this.currentShapeFile = shp
  }

  public whenRelateCoverageFields(relation: CoverageFieldRelationShip) {
    if (this.currentShapeFile !== null) {
      this.batchUploaderComponent?.upload(this.currentShapeFile, relation)
    }
  }
}