import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShapefileRecordViewerComponent } from './shapefile-record-viewer.component';

describe('ShapefileRecordViewerComponent', () => {
  let component: ShapefileRecordViewerComponent;
  let fixture: ComponentFixture<ShapefileRecordViewerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShapefileRecordViewerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShapefileRecordViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
