import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

import { Feature } from '../../models/geojson/feature';
import { ShapeFile } from '../../models/read-shapefile/shapefile';
import { CoverageFieldRelationShip } from '../../models/uploader/coverage-field-relationship';
import { GeometryViewerComponent } from '../geometry-viewer/geometry-viewer.component';

@Component({
  selector: 'app-shapefile-record-viewer',
  templateUrl: './shapefile-record-viewer.component.html',
  styleUrls: ['./shapefile-record-viewer.component.scss']
})
export class ShapefileRecordViewerComponent implements OnInit {

  @Input("shapefile")
  public shapeFile: ShapeFile | null = null

  @Output()
  public whenRelateCoverageFields = new EventEmitter<CoverageFieldRelationShip>();

  @ViewChild(GeometryViewerComponent)
  private geometryViewerComponent: GeometryViewerComponent | undefined;

  public fieldRelationShip: CoverageFieldRelationShip = new CoverageFieldRelationShip()

  public hideViewer: boolean = true

  constructor() { }

  ngOnInit(): void {
  }

  public preserveOriginalOrder = (a: any, b: any) => {
    return 0;
  }

  public showFeature(feature: Feature) {
    this.hideViewer = false
    setTimeout(() => {
      this.geometryViewerComponent?.showFeature(feature), 1000
    })

  }

  public resetComponentStatus() {
    this.geometryViewerComponent?.resetViewer()
    this.hideViewer = true
    this.shapeFile = null
  }

  public relateCoverageFields() {
    this.whenRelateCoverageFields.emit(this.fieldRelationShip)
  }

}
