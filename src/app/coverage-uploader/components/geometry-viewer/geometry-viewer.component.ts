import { AfterViewInit, Component, Input, OnInit } from '@angular/core';

import Map from 'ol/Map';
import View from 'ol/View';
import XYZ from 'ol/source/XYZ'
import { Tile as TileLayer } from 'ol/layer'
import GeoJSON from 'ol/format/GeoJSON';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import { Feature } from '../../models/geojson/feature';

@Component({
  selector: 'app-geometry-viewer',
  templateUrl: './geometry-viewer.component.html',
  styleUrls: ['./geometry-viewer.component.scss']
})
export class GeometryViewerComponent implements OnInit, AfterViewInit {

  @Input()
  public target: string | null = null;

  public map: Map | null = null
  private view: View = new View({
    projection: 'EPSG:4326',
    center: [-70.6072217, -33.4394197],
    zoom: 18
  });

  private googleMapTileLayer = new TileLayer({
    visible: true,
    zIndex: 0,
    source: new XYZ({
      url: 'http://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}'
    })
  })

  private featureSource = new VectorSource({
    features: []
  });

  private featureLayer = new VectorLayer({
    source: this.featureSource,
    zIndex: 3
  })


  constructor() {

  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    if (this.target !== null) {
      this.map = this.initMap()
    }
  }

  private initMap(): Map {

    return new Map({
      target: (this.target as string),
      view: this.view,
      layers: [
        this.googleMapTileLayer,
        this.featureLayer
      ]
    })
  }

  public showFeature(feature: Feature) {
    this.map?.updateSize()
    this.featureSource.clear()
    this.featureSource.addFeature(new GeoJSON().readFeature(feature))
    this.map?.getView().fit(feature.geometry.bbox as [number, number, number, number])
  }

  public resetViewer() {
    this.featureSource.clear()
  }

}
