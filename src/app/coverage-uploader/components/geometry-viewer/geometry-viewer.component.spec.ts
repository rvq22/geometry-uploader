import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeometryViewerComponent } from './geometry-viewer.component';

describe('GeometryViewerComponent', () => {
  let component: GeometryViewerComponent;
  let fixture: ComponentFixture<GeometryViewerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GeometryViewerComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeometryViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
