import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { ShapeFile } from '../../models/read-shapefile/shapefile';
import { ShapeFilePaginator } from '../../util/shapefile-paginator';
import { ZipUtil } from '../../util/zip-util';

@Component({
  selector: 'app-read-shapefile',
  templateUrl: './read-shapefile.component.html',
  styleUrls: ['./read-shapefile.component.scss']
})
export class ReadShapefileComponent implements OnDestroy {

  @Output("onReadShapeFile")
  private onReadShapeFile = new EventEmitter<ShapeFile | null>();

  private fileInputId: string = "file-input"

  private fileSizeCriteria: number = 70 * 1000 * 1000

  private paginator: ShapeFilePaginator | undefined = undefined

  private shpBlob: Blob | undefined = undefined
  private dbfBlob: Blob | undefined = undefined

  constructor() { }

  ngOnDestroy() {
    this.clearBlobs()
  }

  private clearBlobs() {
    this.shpBlob = undefined
    this.dbfBlob = undefined
  }

  private isLargeFile(shpFile: File): boolean {

    if (shpFile.size > this.fileSizeCriteria) {
      return true;
    }
    return false
  }

  async readZip() {
    let fileInput = <HTMLInputElement>document.getElementById(this.fileInputId)

    if (fileInput.files !== null && fileInput.value.length !== 0) {
      await this.doWhenReadFinished(fileInput.files[0])
      fileInput.value = "";

    }
  }

  async doWhenReadFinished(file: File) {
    await this.setBlobs(file);

    await this.initPaginator()
  }

  async setBlobs(selectedFile: File, filenameEncoding: string = "utf-8") {

    this.clearBlobs()

    await ZipUtil.forEachEntryBlobs(selectedFile, filenameEncoding, async (filename: string, blob: Blob) => {

      if (filename.endsWith(".shp")) {
        console.log(filename)
        this.shpBlob = blob
      }

      if (filename.endsWith(".dbf")) {
        console.log(filename)
        this.dbfBlob = blob
      }
    })
  }

  private async readShapeFile(shpFile: File) {
    //await this.onReadShapeFile.emit(await ShapeFileReader.read(shpFile))
  }

  private isBlobInit() {
    return this.shpBlob !== undefined
  }

  private async initPaginator() {
    this.paginator = await ShapeFilePaginator.initPaginator(<Blob>this.shpBlob, <Blob>this.dbfBlob)
  }

  public nextPage() {
    if (this.isBlobInit()) {
      this.paginator?.getNext(<Blob>this.shpBlob, <Blob>this.dbfBlob)
    }
  }

  public previousPage() {
    if (this.isBlobInit()) {
      this.paginator?.getPrevious(<Blob>this.shpBlob, <Blob>this.dbfBlob)
    }
  }

}
