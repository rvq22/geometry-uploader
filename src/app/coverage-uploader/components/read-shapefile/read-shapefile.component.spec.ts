import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadShapefileComponent } from './read-shapefile.component';

describe('ReadShapefileComponent', () => {
  let component: ReadShapefileComponent;
  let fixture: ComponentFixture<ReadShapefileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReadShapefileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadShapefileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
