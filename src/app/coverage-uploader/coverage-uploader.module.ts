import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { BatchUploaderComponent } from "./components/batch-uploader/batch-uploader.component";
import { GeometryViewerComponent } from "./components/geometry-viewer/geometry-viewer.component";
import { CoverageUploaderComponent } from "./components/coverage-uploader/coverage-uploader.component";
import { ReadShapefileComponent } from "./components/read-shapefile/read-shapefile.component";
import { ShapefileRecordViewerComponent } from "./components/shapefile-record-viewer/shapefile-record-viewer.component";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

const COMPONENTS = [
    CoverageUploaderComponent,
    GeometryViewerComponent,
    BatchUploaderComponent,
    ReadShapefileComponent,
    ShapefileRecordViewerComponent,
]

@NgModule({
    imports: [CommonModule, FormsModule, HttpClientModule],
    declarations: [...COMPONENTS],
    exports: [...COMPONENTS]
})
export class CoverageUploaderModule {

}