import { NgModule } from "@angular/core";
import { ContentWrapperComponent } from "./components/content-wrapper/content-wrapper.component";
import { NavbarComponent } from "./components/navbar/navbar.component";

const COMPONENTS = [
    NavbarComponent,
    ContentWrapperComponent
]

@NgModule({
    declarations: [...COMPONENTS],
    exports: [...COMPONENTS]
})
export class CoreModule {

}