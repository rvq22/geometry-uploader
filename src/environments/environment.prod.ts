class BackendConfig {
  url: string
  endpoints: {
    uploadCoverage: string
  }

  constructor() {
    this.url = "http://localhost:8080/fu"

    this.endpoints = {
      uploadCoverage: this.url + "/coverage/upload"
    }

  }
}

export const environment = {
  production: true,
  backend: new BackendConfig()
};
